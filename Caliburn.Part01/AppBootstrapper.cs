﻿using System.Windows;
using Caliburn.Part01.ViewModels;
using Caliburn.Micro;

namespace Caliburn.Part01
{
    public class AppBootstrapper : BootstrapperBase
    {
        public AppBootstrapper()
        {
            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<AppViewModel>();
        }
    }
}
