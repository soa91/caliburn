﻿using System.Windows;
using Caliburn.Part02.ViewModels;
using Caliburn.Micro;

namespace Caliburn.Part02
{
    public class AppBootstrapper : BootstrapperBase
    {
        public AppBootstrapper()
        {
            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<AppViewModel>();
        }
    }
}
