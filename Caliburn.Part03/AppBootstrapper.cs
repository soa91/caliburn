﻿using System.Windows;
using Caliburn.Part03.ViewModels;
using Caliburn.Micro;

namespace Caliburn.Part03
{
    public class AppBootstrapper : BootstrapperBase
    {
        public AppBootstrapper()
        {
            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<AppViewModel>();
        }
    }
}
