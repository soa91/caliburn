﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Caliburn.Part04.Messages
{
    public class ColorEvent
    {
        public SolidColorBrush Color { get; private set; }

        public ColorEvent(SolidColorBrush color)
        {
            Color = color;
        }
    }
}
