﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Caliburn.Micro;
using Caliburn.Part04.Messages;

namespace Caliburn.Part04.ViewModels
{
    [Export(typeof(ColorViewModel))]
    public class ColorViewModel : PropertyChangedBase
    {
        private readonly IEventAggregator _events;
        [ImportingConstructor]
        public ColorViewModel(IEventAggregator events)
        {
            _events = events;
        }


        public void Green()
        {
            _events.PublishOnUIThread(new ColorEvent(new SolidColorBrush(Colors.Green)));
        }


        public void Yellow()
        {
            _events.PublishOnUIThread(new ColorEvent(new SolidColorBrush(Colors.Yellow)));
        }

        public void Red()
        {
            _events.PublishOnUIThread(new ColorEvent(new SolidColorBrush(Colors.Red)));
        }

    }
}
