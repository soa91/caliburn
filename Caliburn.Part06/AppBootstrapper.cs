﻿using System.Windows;
using Caliburn.Part06.ViewModels;
using Caliburn.Micro;

namespace Caliburn.Part06
{
    public class AppBootstrapper : BootstrapperBase
    {
        public AppBootstrapper()
        {
            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<AppViewModel>();
        }
    }
}
