﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caliburn.Part07.MultiView.Messages
{
    public class ClickEvent
    {
        public ClickEvent(string text)
        {
            ClickText = text;
        }

        public string ClickText { get; private set; }
    }
}
