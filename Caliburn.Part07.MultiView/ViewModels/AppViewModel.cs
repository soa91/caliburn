﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Caliburn.Part07.MultiView.Messages;

namespace Caliburn.Part07.MultiView.ViewModels
{
    [Export(typeof(AppViewModel))]
    public class AppViewModel : Conductor<object>, IHandle<ClickEvent>
    {
        [ImportingConstructor]
        public AppViewModel(BlackViewModel child1, SendMessagesViewModel child2, IEventAggregator events)
        {
            Child = child1;
            ButtonBlock = child2;

            events.Subscribe(this);
        }

        public BlackViewModel Child { get; private set; }

        public SendMessagesViewModel ButtonBlock { get; private set; }

        private string _textEvent = string.Empty;

        public string TextEvent
        {
            get { return _textEvent; }
            private set
            {
                if (_textEvent != value)
                {
                    _textEvent = value;
                    NotifyOfPropertyChange(() => TextEvent);
                }
            }
        }

        public void ShowRedScreen()
        {
            ActivateItem(new RedViewModel());
        }

        public void ShowGreenScreen()
        {
            ActivateItem(new GreenViewModel());
        }

        public void ShowBlueScreen()
        {
            ActivateItem(new BlueViewModel());
        }

        public void Handle(ClickEvent message)
        {
            TextEvent = message.ClickText;
        }
    }
}
