﻿using System.ComponentModel.Composition;
using Caliburn.Micro;

namespace Caliburn.Part07.MultiView.ViewModels
{
    [Export(typeof(BlackViewModel))]
    public class BlackViewModel : Screen
    {
        private string _screenName = "Black";

        public string ScreenName
        {
            get { return _screenName; }
            set
            {
                if (_screenName != value)
                {
                    _screenName = value;
                    NotifyOfPropertyChange(() => ScreenName);
                }
            }
        }
    }
}
