﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Caliburn.Part07.MultiView.Messages;

namespace Caliburn.Part07.MultiView.ViewModels
{
    [Export(typeof(SendMessagesViewModel))]
    public class SendMessagesViewModel : Screen
    {
        private readonly IEventAggregator _events;

        [ImportingConstructor]
        public SendMessagesViewModel(IEventAggregator events)
        {
            _events = events;
        }

        public void ClickEvent1()
        {
            _events.PublishOnUIThread(new ClickEvent("Click 1"));
        }

        public void ClickEvent2()
        {
            _events.PublishOnUIThread(new ClickEvent("Click 2"));
        }

        public void ClickEvent3()
        {
            _events.PublishOnUIThread(new ClickEvent("Click 3"));
        }
    }
}
