﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caliburn.Part08.TestBackgroundColor.ViewModels
{
    [Export(typeof(AppViewModel))]
    public class AppViewModel 
    {
        public TestBackViewModel TestBack { get; private set; }
        [ImportingConstructor]
        public AppViewModel(TestBackViewModel testBack)
        {
            TestBack = testBack;
        }
    }
}
