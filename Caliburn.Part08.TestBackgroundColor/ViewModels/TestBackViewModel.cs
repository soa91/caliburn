﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;

namespace Caliburn.Part08.TestBackgroundColor.ViewModels
{
    [Export(typeof(TestBackViewModel))]
    public class TestBackViewModel : Screen
    {
        private decimal _sheetSizeWidth;

        public decimal SheetSizeWidth
        {
            get { return _sheetSizeWidth; }
            private set
            {
                if (_sheetSizeWidth != value)
                {
                    _sheetSizeWidth = value;
                    NotifyOfPropertyChange(() => SheetSizeWidth);
                }
            }
        }

        private decimal _sheetSizeHeight;
        public decimal SheetSizeHeight
        {
            get { return _sheetSizeHeight; }
            private set
            {
                if (_sheetSizeHeight != value)
                {
                    _sheetSizeHeight = value;
                    NotifyOfPropertyChange(() => SheetSizeHeight);
                }
            }
        }
        [ImportingConstructor]
        public TestBackViewModel()
        {
            SheetSizeWidth = 5;
            SheetSizeHeight = 5;
        }

        public void UpdateSize()
        {
            SheetSizeWidth = 110;
            SheetSizeHeight = 15;
        }
    }
}
