﻿using System.Windows;
using Caliburn.Micro;
using Caliburn.Part09.Tabs.ViewModels;

namespace Caliburn.Part09.Tabs
{
    public class AppBootstrapper : BootstrapperBase
    {
        public AppBootstrapper()
        {
            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<AppViewModel>();
        }
    }
}
