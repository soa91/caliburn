﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caliburn.Part09.Tabs.Models
{
    public class ComponentModel
    {
        public string Value { get; set; }
        public string Temp { get; set; }
    }
}
