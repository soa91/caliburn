﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caliburn.Part09.Tabs.Models
{
    public class CompositModel
    {
        public string Prop1 { get; set; }   
        public List<ComponentModel> Children { get; set; } = new List<ComponentModel>();
    }
}
