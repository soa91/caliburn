﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Caliburn.Part09.Tabs.Models;

namespace Caliburn.Part09.Tabs.ViewModels
{
    public class AppViewModel : Conductor<IScreen>.Collection.OneActive
    {
        int count = 1;

        private CompositModel _model;

        public AppViewModel()
        {
            _model = new CompositModel {Prop1 = "Test"};

            var comp1 = new ComponentModel
            {
                Value = "1",
                Temp = "asd"
            };

            var comp2 = new ComponentModel
            {
                Value = "2",
                Temp = "qwe"
            };

            _model.Children.Add(comp1);
            _model.Children.Add(comp2);

            foreach (var componentModel in _model.Children)
            {
                Items.Add(new TabViewModel()
                {
                    DisplayName = "Tab " + count++,
                    PropValue = componentModel.Value,
                    PropT = componentModel.Temp
                });
            }
        }

        public void OpenTab()
        {
            ActivateItem(new TabViewModel
            {
                
            });
        }

        public void Accept()
        {
            _model.Children.Clear();

            foreach (var screen in Items)
            {
                var tab = (TabViewModel)screen;
                _model.Children.Add(new ComponentModel()
                {
                    Value = tab.PropValue,
                    Temp = tab.PropT
                });
            }
        }
    }
}