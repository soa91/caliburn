﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;

namespace Caliburn.Part09.Tabs.ViewModels
{
    class TabViewModel : Screen 
    {
        public string PropValue { get; set; }
        public string PropT { get; set; }   
    }
}
