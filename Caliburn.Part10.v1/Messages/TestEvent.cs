﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caliburn.Part10.CSThreading1.Messages
{
    public enum MyEnum
    {
        Runing1,
        Runing2,
        Runing3,
        Stop
    }

    public class TestEvent
    {
        public MyEnum Stat { get; private set; }

        public TestEvent(MyEnum stat)
        {
            Stat = stat;
        } 
    }
}
