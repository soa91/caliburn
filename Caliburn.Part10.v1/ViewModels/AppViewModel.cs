﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;
using Caliburn.Micro;
using Caliburn.Part10.CSThreading1.Messages;

namespace Caliburn.Part10.CSThreading1.ViewModels
{
    public class Worker
    {
        private bool _canceled = false;

        public void Cancel()
        {
            _canceled = true;
        }

        public void Work()
        {
            for (int i = 0; i < 3; i++)
            {
                if (_canceled) break;

                Thread.Sleep(500);
                ProcessChanged?.Invoke(i);
            }

            WorkComplited?.Invoke(_canceled);
        }

        public event Action<int> ProcessChanged;
        public event Action<bool> WorkComplited;
    }

    [Export(typeof(AppViewModel))]
    public class AppViewModel : PropertyChangedBase
    {
        public Worker _worker;
        private readonly IEventAggregator _events;
        public StatusBarViewModel StatusBar { get; set; }

        [ImportingConstructor]
        public AppViewModel(StatusBarViewModel statusBarViewModel, IEventAggregator events)
        {
            _events = events;
            StatusBar = statusBarViewModel;
        }       

        public void Run1()
        {
            _worker = new Worker();
            _worker.ProcessChanged += _worker_ProcessChanged;
            _worker.WorkComplited += _worker_WorkComplited;

            Thread thread = new Thread(_worker.Work);
            thread.Start();
        }

        private void _worker_ProcessChanged(int obj)
        {
            if (obj == 0) _events.PublishOnUIThread(new TestEvent(MyEnum.Runing1));
            if (obj == 1) _events.PublishOnUIThread(new TestEvent(MyEnum.Runing2));
            if (obj == 2) _events.PublishOnUIThread(new TestEvent(MyEnum.Runing3));
        }

        private void _worker_WorkComplited(bool obj)
        {
            if (obj)
            {
                _events.PublishOnUIThread(new TestEvent(MyEnum.Stop));
            }
            else
            {
                _events.PublishOnUIThread(new TestEvent(MyEnum.Stop));
            }
        }

        public void Run2()
        {
            _events.PublishOnUIThread(new TestEvent(MyEnum.Runing2));
        }

        public void Run3()
        {
            _events.PublishOnUIThread(new TestEvent(MyEnum.Runing3));
        }

        public void Stop()
        {
            _events.PublishOnUIThread(new TestEvent(MyEnum.Stop));
        }
    }
}
