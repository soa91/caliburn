﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Caliburn.Part10.CSThreading1.Messages;

namespace Caliburn.Part10.CSThreading1.ViewModels
{
    [Export(typeof(StatusBarViewModel))]
    public class StatusBarViewModel : Screen, IHandle<TestEvent>
    {
        private string _status = "";

        public string Status
        {
            get { return _status; }
            private set
            {
                if (!_status.Equals(value))
                {
                    _status = value;
                    NotifyOfPropertyChange(() => Status);
                }
            }
        }

        [ImportingConstructor]
        public StatusBarViewModel(IEventAggregator events)
        {
            events.Subscribe(this);
        }

        public void Handle(TestEvent message)
        {
            switch (message.Stat)
            {
                case MyEnum.Runing1:
                    Status = "Run1";
                    break;
                case MyEnum.Runing2:
                    Status = "Run2";
                    break;
                case MyEnum.Runing3:
                    Status = "Run3";
                    break;
                case MyEnum.Stop:
                    Status = "Stop";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
